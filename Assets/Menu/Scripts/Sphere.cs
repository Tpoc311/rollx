﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sphere : MonoBehaviour {

	public float speed, tilt;
	public Vector3 target;
	// Update is called once per frame
	void Update () 
	{
		transform.position = Vector3.MoveTowards (transform.position, target, Time.deltaTime * speed);
		if (transform.position == target && target.y != -1.5f)
			target.y = -1.5f;
		else if (transform.position == target && target.y == -1.5f)
			target.y = 0.2f;
		transform.Rotate (Vector3.up * tilt);
	}
}
